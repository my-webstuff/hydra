;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((emacs-lisp-mode . ((lua-indent-level . 4)
					 (indent-tabs-mode . t)
					 (tab-width . 4)))
 (fennel-mode . ((firestarter . "./build.sh;notify-send build done! ")
				 (whitespace-mode . t))))
