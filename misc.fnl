(local misc {})

(fn misc.hr []
  (.. "\n\t\t\t" "<hr>")
  )

(fn misc.bold [text]
  (.. "<b>" text "</b>")
  )


(fn misc.strong [text]
  (.. "<strong>" text "</strong>")
  )

(fn styleFmt [style]
  (.. " style=\"" style "\"")
  )

(fn misc.span [text style]
  (.. "<span" (styleFmt style) ">" text "</span>")
  )

(fn misc.small [text style]
  (.. "<small" (styleFmt style) ">" text "</small>")
  )




;;(fn misc.pre [content])








misc
