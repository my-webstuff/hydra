#!/usr/bin/env fennel

(local hydra [])

(fn hydra.preview [content]
  (print content)
)


(fn hydra.writeFile [content filename]
  (with-open [fout (io.open filename :w) ]
	(fout:write content)
	) ; => first line of index.html
  (print (.. "wrote " "data " "to " filename))
  )

hydra
