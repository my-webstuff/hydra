#!/usr/bin/env fennel
(local tags [])
(local tcat table.concat)
;; "table to store tags, think of it as an object or class"

(fn pp [...]
  (let [fnl (require :fennel)]
	(fnl.view ...)))


(fn tags.htag [lvl text]
  "creates the equivalent html-tag <h[i]>"
  (.. "\t\t<h" lvl "> " text " </h" lvl ">\n"))

(fn tags.h [lvl text]
  "creates the equivalent html-tag <h[i]>"
	;;(for [i 1 lvl] (set snippet (.. snippet "#")))
	;;(.. "\t\t<h" lvl "> " snippet " " text " </h" lvl ">\n")
  (let [snip (string.rep "#" lvl)]
	(tags.htag lvl (.. snip " " text))))



(for [i 1 6]
  (tset
   tags (.. :h (tostring i))
   (lambda [text]
	 {:fnl/docstring (.. "shorthand h" i " tag with nice styling.")}
	 (tags.h i text)))
  (tset
   tags (.. :h (tostring i) :tag)
   (lambda [text]
	 {:fnl/docstring (.. "shorthand h" i "tag tag.")}
	 (tags.htag i text))))


(lambda tags.html [{:head ?head : body :foot ?foot}]
  "creates the equivalent html-tag, this is the top-most tag on the page.
 it takes a table of tables containing at least a body index
 but will also work with head and foot indexes"
  (tcat ["<!DOCTYPE html>\n"
		 :<html>
		 ;;(if (not= input.head nil) (tcat input.head) "")
		 (or (tcat ?head) "")
		 (tcat body)
		 ;;(if (not= (. input :foot) nil) (tcat (. input :foot)) "")
		 (or (tcat ?foot) "")
		 :</html>] "\n"))

(fn tags.body [class content]
  "creates a div tag that takes table;content and string;class as input. it 'wraps' content"
  (tcat ["<body "
		 "class=\""
		 class
		 "\">\n"
		 (tcat content)
		 "\n</body>\n"]))

;; <footer>
;;   <p>Author: Hege Refsnes</p>
;;   <p><a href="mailto:hege@example.com">hege@example.com</a></p>
;; </footer>

;; The <footer> tag defines a footer for a document or section.
;; A <footer> element typically contains:

;; authorship information
;; copyright information
;; contact information
;; sitemap
;; back to top link
;; related documents
;; You can have several <footer> elements in one document.
;;<<< from https://www.w3schools.com/tags/tag_footer.asp

(fn tags.footer [content]
  "The <footer> tag defines a footer for a document or section."
  (.. "<footer>\n" (tcat content) "\n</footer>\n"))

;;(fn tags.foot [data]
;; [
;;  (tags.div "contact"
;;            [
;;             (tags.p [(tags.a (.. "mailto:" (. data :mail)) "mail")] )
;;             (tags.p [(tags.a (. data :license)  "license")])
;;             ]
;;            )
;;  ]
;; )

(fn tags.a [url text ...]
  "creates the equivalent html-tag. this is a link"
  (local input [...])
  ;;(print "1 2 3 4 5 6 7 8 9 >>>>")
  ;;  (print (or (. (. input 1) :class) ""))
  ;;(print ">>>>>1 2 3 4 5 6 7 8 9 \n")
  ;;  (print (or (. (. input 1) :class) "###no data here###") )
  (local class
		 (if (= (. input 1 :class) nil) "class=\"other\" "
			 (.. "class=\"" (. input 1 :class) "\" ")))
  (.. "<a " class "href=\"" url "\">" text :</a> "\n"))

(fn tags.img [url alt width height text]
  "creates the equivalent html-tag. this is an image"
  (.. "\t\t" "<img src=\"" url "\" alt=\"" alt "\" width=\"" width
	  "\" height=\"" height "\">" "\n"))

(fn tags.image [image]
  (tags.img image.url image.alt image.width image.height (or image.text " ")))

(fn tags.li [content]
  "creates the equivalent html-tag. list item,this often contains an atag creating a list of links"
  (.. "\t\t<li  >" content "</li>\n"))

(fn tags.li1 [content input]
  "creates the equivalent html-tag. list item,this often contains an atag creating a list of links"
  (local class (if (= input.class nil) " "
				   (.. "class=\"" input.class "\" ")))
  (.. "\t\t<li " class " >" content "</li>\n"))

(fn tags.meta [content] (.. "\t<meta " content ">\n"))

(fn tags.title [title] (.. "\n\t<title> " title " </title>\n"))

(fn tags.head [content]
  "creates the equivalent html-tag"
  (.. "<head>\n" (tcat content) :</head>))

(fn tags.stylesheet [stylesheet] (.. "<link rel=" "\"stylesheet\" " "href=\"" stylesheet "\">"))

(fn tags.feedlink [args]
  (tcat
   [:<link
	"rel=\"alternate\""
	"type=\"application/atom+xml\""
	"title=\""
	args.title
	"\""
	"href=\" "
	args.url
	" \" />"]))


(tset tags :tbl {})
;;; table tag
(fn tags.tbl.head [text]
  (.. "\t\t\t<th> " text " </th>\n"))

(fn tags.tbl.data [text]
  (.. "\t\t\t<td> " text " </td>\n"))

(fn tags.tbl.row [cells]
  (.. "\t\t<tr>\n" (tcat cells) "\t\t</tr>\n"))

(fn tags.tbl.table [tbl]
  (local data [])
  (local heads [])
  (each [key value (ipairs (. tbl :headers))]
	(table.insert heads (tags.tbl.head value)))
  (local rows [])
  (each [key value (ipairs (. tbl :rows))]
	(local row [])
	(each [k v (ipairs value)]
	  (table.insert row (tags.tbl.data v)))
	(table.insert rows (tags.tbl.row row)))
  (tcat
   [:<table
	(if (not= (. tbl :class) nil)
		(.. " class=\"" (. tbl :class) "\"") "")
	(if (not= (. tbl :id) nil) (.. " id=\"" (. tbl :id) "\"") "")
	">\n"
	(tags.tbl.row heads)
	(tcat rows)
	:</table>]))




(fn tags.linkList [input]
  "builds an unordered list containing list-items containing links, a list of links"
  (local retval [])
  (each [k v (pairs input)]
	(table.insert
	 retval
	 (tags.li
	  (tags.a
	   v.url v.text
	   {:class (or (. v :class) :links)}))))
  (tcat ["\t<ul>\n" (tcat retval) "\t</ul>"]))

(fn tags.navigation [input]
  "builds an unordered list containing list-items containing links, a list of links"
  (local retval [])
  (each [k v (pairs input)]
	(table.insert retval (tags.li1 (tags.a (. v :url) (. v :text) v) v)))
  (tcat ["\t<ul class=\"navigation\">\n"
		 (tcat retval)
		 "\n\t</ul>"]))

(fn tags.div [class content]
  "creates a div tag that takes table;content and string;class as input. it 'wraps' content"
  (tcat ["\t<div "
		 "class=\""
		 class
		 "\">\n"
		 (tcat content)
		 "\n\t</div>\n"]))

(fn tags.nav [class content]
  "creates a nav tag that takes table;content and string;class as input. it 'wraps' content"
  (tcat ["\t<nav "
		 "class=\""
		 class
		 "\" >\n"
		 (tcat content)
		 "\n\t</nav>\n"]))

(fn tags.section [class content]
  "creates a section tag that takes table;content and string;class as input. it 'wraps' content this is verry simlar to div"
  (tcat ["\t<section "
		 "class=\""
		 class
		 "\">\n"
		 (tcat content)
		 "\n\t</section>\n"]))

(fn tags.article [class content]
  "creates a section tag that takes table;content and string;class as input. it 'wraps' content this is verry simlar to div"
  (tcat ["\t<article "
		 "class=\""
		 class
		 "\">\n"
		 (tcat content)
		 "\n\t</article>\n"]))

(fn tags.p [content]
  (tcat ["\t\t<p>\n\t\t\t"
		 (tcat content "</br>\n\t\t\t")
		 "\n\t\t</p>\n"]))


tags
